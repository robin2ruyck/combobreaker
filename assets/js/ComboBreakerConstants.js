class ComboBreakerConstants {

	constructor () {
		/**
		* Moveset
		*/
		this.moveSetInterval = {min: 1, max:4}

		this.feed  = 1;
		this.brawl = 2;
		this.sword = 3;
		this.gun   = 4;

		/**
		* Moveset colors
		*/
		this.feedColor  = 'red';
		this.brawlColor = 'blue';
		this.swordColor = 'yellow';
		this.gunColor	= 'purple';

		/**
		* Table dimensions
		*/
		this.tableX 	 = 500; // px
		this.tableY 	 = 500; // px
		this.tableCasesX = 5;
		this.tableCasesY = 5;
	}
}
