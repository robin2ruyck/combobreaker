class ComboBreakerTable extends ComboBreakerConstants {

	constructor (dimX, dimY, canvasId) {
		super()
		this.dimX     	   = dimX;
		this.dimY     	   = dimY;
		this.table    	   = new Array();
		this.canvasId 	   = canvasId;
		this.canvas   	   = null;
		this.canvasContext = null;
	}

    initializeTable() 
    {
	    for (let i = 0; i < this.tableCasesX; i++) {
	    	this.table.push(this.generateLine());
	    }

		this.cvs 		   = document.getElementById(this.canvasId);
		this.canvasContext = this.cvs.getContext('2d');
	    this.cvs.width     = 500;
	    this.cvs.height    = 500;



	    this.fillTable(0,0);
    }

    generateLine()
    {
        let line = [];

        for (let i = 0; i < this.tableCasesX; i++) {
            line.push(this.generateCase());
        }

        return line;
    }

    generateCase()
    {
        let min = this.moveSetInterval.min;
        let max = this.moveSetInterval.max + 1;

        return Math.floor(Math.random() * (max - min)) + min;
    }

    fillTable(startX, startY)
    {
        let ctx  = this.canvasContext;
        let newX = startX;
        let newY = startY;

        for (let i = 0; i < this.tableCasesX; i++) {
            for (let j = 0; j < this.tableCasesY; j++) {

                let tabValue =  this.table[i][j];

                if (tabValue == this.feed) {
                    ctx.fillStyle = this.feedColor;
                }
                else if (tabValue == this.brawl) {
                    ctx.fillStyle = this.brawlColor;
                }
                else if (tabValue == this.sword) {
                    ctx.fillStyle = this.swordColor;
                } 
                else if (tabValue == this.gun) {
                    ctx.fillStyle = this.gunColor;
                }

                ctx.fillRect(
                    newX,
                    newY,
                    this.tableX / this.tableCasesX,
                    this.tableY / this.tableCasesY
                );
                newX = newX + 100;
            }
                newY = newY + 100;
                newX = startX;
        }
    }
}
